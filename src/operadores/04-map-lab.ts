import { fromEvent } from 'rxjs';
import { map, tap } from 'rxjs/operators';

const texto = document.createElement('div');
texto.innerHTML = `
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nunc nunc, euismod non magna sed, molestie cursus mi. Aliquam in aliquet leo, quis ultricies velit. Nulla sed nisi venenatis, ultrices dui et, ultrices tellus. Duis tincidunt mauris vel ligula pharetra, in ullamcorper nulla ornare. In et vehicula metus. Suspendisse potenti. Maecenas orci elit, maximus non dui in, semper tincidunt nibh. Donec tincidunt metus venenatis purus lobortis pretium. Mauris non augue congue, pulvinar libero eget, bibendum lectus. Duis placerat euismod risus, accumsan pellentesque massa pulvinar non. Sed turpis tellus, molestie ac urna id, imperdiet euismod nisl. Nunc id blandit nisl, sed gravida metus. Aliquam erat volutpat. Nunc commodo urna vitae efficitur porttitor.
<br/><br/>
Etiam gravida, massa sit amet fermentum tempus, turpis mi semper nulla, vitae efficitur lacus ante ut dolor. Nullam tempus interdum mauris. Aenean tincidunt leo non nisl congue eleifend. Aenean accumsan ligula dui, eget maximus turpis condimentum a. Maecenas nunc odio, feugiat et euismod sed, tincidunt lacinia sapien. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce hendrerit nibh quis ligula posuere fringilla. Morbi volutpat imperdiet nunc sed consequat. Curabitur congue ut turpis dignissim gravida. Mauris pretium dolor ac mi ullamcorper malesuada. Cras imperdiet ac ex nec sodales.
<br/><br/>
Suspendisse quis eros sed nisl bibendum auctor. Nunc metus enim, maximus id ante quis, faucibus posuere lacus. Maecenas tellus sapien, varius vitae magna et, vulputate pharetra orci. Vestibulum tempus congue metus, non convallis justo. Morbi fringilla sem at dolor faucibus congue. In porttitor nulla scelerisque enim sagittis blandit. Nulla tempus pulvinar pulvinar. Suspendisse at pellentesque felis. Donec dapibus nunc in erat auctor vestibulum.
<br/><br/>
Nunc sed lacus fermentum, porttitor nunc in, pellentesque nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi mi sem, luctus nec urna a, dictum viverra felis. Ut ac tellus gravida, eleifend orci sed, dapibus ipsum. Proin mauris augue, mattis consequat condimentum a, ullamcorper non nisi. Morbi non lacinia ligula, nec fringilla lorem. Pellentesque eget velit convallis, rutrum metus id, interdum ligula. Suspendisse mollis odio vel nulla viverra, sed posuere tellus lobortis. Ut efficitur massa at arcu viverra, eu elementum sem varius. In feugiat accumsan sapien vitae mollis. Vestibulum faucibus lobortis dolor, sed pulvinar libero consequat at.
<br/><br/>
Cras tempus fringilla nisi. Suspendisse tempor ultrices tortor, ut ultrices leo lobortis non. Duis ipsum ligula, convallis ac nulla a, bibendum scelerisque turpis. Vivamus ultricies varius tortor id consequat. Maecenas ac malesuada quam. Nullam sed diam quis purus pharetra fermentum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; 
`;

const body = document.querySelector('body');
body.append(texto);

const progressBar = document. createElement('div');
progressBar.setAttribute('class', 'progress-bar');
body.append(progressBar);

const calcularPorcentajeScroll = ( event ) => {
    const {

        scrollTop,
        scrollHeight,
        clientHeight

    } = event.target.documentElement;

    return ( scrollTop / ( scrollHeight - clientHeight )) * 100;
    
}

const scroll$ = fromEvent( document, 'scroll' );
// scroll$.subscribe(console.log);

const progress$ = scroll$.pipe(
    // map( event => calcularPorcentajeScroll(event) )
    map( calcularPorcentajeScroll ),
    tap( console.log )

    );


progress$.subscribe( porcentaje => {

    progressBar.style.width = `${porcentaje}%`;

});